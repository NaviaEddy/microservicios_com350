const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/bd_usuarios', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})
.then(() => {
  console.log('Conectado exitosamente a MongoDB');
})
.catch((err) => {
  console.error('Error al conectar a MongoDB', err);
});
